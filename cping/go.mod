module cping

go 1.15

require (
	github.com/go-ping/ping v0.0.0-20201115131931-3300c582a663
	github.com/gookit/color v1.3.5
	github.com/parnurzeal/gorequest v0.2.16 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	moul.io/http2curl v1.0.0 // indirect
)
